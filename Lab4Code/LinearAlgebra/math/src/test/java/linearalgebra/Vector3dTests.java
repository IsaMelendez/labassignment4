//Isa Melendez, 2237929

package linearalgebra;

import org.junit.Test;

import static org.junit.Assert.*;

public class Vector3dTests {
    @Test
    public void testVector3d_getValues(){
        Vector3d vector = new Vector3d(3, 2, 4);

        assertEquals(3, vector.getX(), 0.0001);
        assertEquals(2, vector.getY(), 0.0001);
        assertEquals(4, vector.getZ(), 0.0001);
    }

    @Test
    public void testVector3d_magnitude(){
        Vector3d vector = new Vector3d(4, 2, 1);

        assertEquals(4.5825, vector.magnitude(), 0.0001);
    }

    @Test
    public void testVector3d_dotProduct(){
        Vector3d vector1 = new Vector3d(1, 2, 4);
        Vector3d vector2 = new Vector3d(3, 10, 5);

        assertEquals(43, vector1.dotProduct(vector2), 0.0001);
    }

    @Test
    public void testVector3d_add(){
        Vector3d vector1 = new Vector3d(3, 1, 4);
        Vector3d vector2 = new Vector3d(6, 2, 10);

        Vector3d expected = vector1.add(vector2);

        assertEquals(9, expected.getX(), 0.0001);
        assertEquals(3, expected.getY(), 0.0001);
        assertEquals(14, expected.getZ(), 0.0001);
    }
}
