//Isa Melendez, 2237929

package linearalgebra;
import java.lang.Math;

public class Vector3d{
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX(){
        return this.x;
    }

    public double getY(){
        return this.y;
    }

    public double getZ(){
        return this.z;
    }

    public double magnitude(){
        return Math.sqrt(Math.pow(x,2) + Math.pow(y,2) + Math.pow(z,2));
    }

    public double dotProduct(Vector3d vector){
        return (this.x * vector.getX() + this.y * vector.getY() + this.z * vector.getZ());
    }

    public Vector3d add(Vector3d vector){
        return new Vector3d(this.x + vector.getX(), this.y + vector.getY(), this.z + vector.getZ());
    }
}